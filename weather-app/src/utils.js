export const availableLocations = [
  {
    cityNameAPI: "%E5%AE%9C%E8%98%AD%E7%B8%A3",
    cityName: "宜蘭縣",
    locationName: "蘇澳",
    sunriseCityName: "宜蘭",
  },
  {
    cityNameAPI: "%E5%98%89%E7%BE%A9%E5%B8%82",
    cityName: "嘉義市",
    locationName: "嘉義",
    sunriseCityName: "嘉義",
  },
  {
    cityNameAPI: "%E5%B1%8F%E6%9D%B1%E7%B8%A3",
    cityName: "屏東縣",
    locationName: "恆春",
    sunriseCityName: "屏東",
  },
  {
    cityNameAPI: "%E9%9B%B2%E6%9E%97%E7%B8%A3",
    cityName: "雲林縣",
    locationName: "麥寮",
    sunriseCityName: "雲林",
  },
  {
    cityNameAPI: "%E8%87%BA%E6%9D%B1%E7%B8%A3",
    cityName: "臺東縣",
    locationName: "臺東",
    sunriseCityName: "臺東",
  },
  {
    cityNameAPI: "%E8%87%BA%E5%8C%97%E5%B8%82",
    cityName: "臺北市",
    locationName: "大安森林",
    sunriseCityName: "臺北",
  },
  {
    cityNameAPI: "%E9%87%91%E9%96%80%E7%B8%A3",
    cityName: "金門縣",
    locationName: "金門",
    sunriseCityName: "金門",
  },
  {
    cityNameAPI: "%E6%A1%83%E5%9C%92%E5%B8%82",
    cityName: "桃園市",
    locationName: "新屋",
    sunriseCityName: "桃園",
  },
  {
    cityNameAPI: "%E5%BD%B0%E5%8C%96%E7%B8%A3",
    cityName: "彰化縣",
    locationName: "彰師大",
    sunriseCityName: "彰化",
  },
  // {
  //   cityName: '嘉義縣',
  //   locationName: '阿里山',
  //   sunriseCityName: '嘉義',
  // },
  {
    cityNameAPI: "%E9%AB%98%E9%9B%84%E5%B8%82",
    cityName: "高雄市",
    locationName: "高雄",
    sunriseCityName: "高雄",
  },
  {
    cityNameAPI: "%E5%9F%BA%E9%9A%86%E5%B8%82",
    cityName: "基隆市",
    locationName: "基隆",
    sunriseCityName: "基隆",
  },
  {
    cityNameAPI: "%E8%87%BA%E5%8D%97%E5%B8%82",
    cityName: "臺南市",
    locationName: "永康",
    sunriseCityName: "臺南",
  },
  {
    cityNameAPI: "%E5%8D%97%E6%8A%95%E7%B8%A3",
    cityName: "南投縣",
    locationName: "日月潭",
    sunriseCityName: "南投",
  },
  {
    cityNameAPI: "%E8%87%BA%E4%B8%AD%E5%B8%82",
    cityName: "臺中市",
    locationName: "臺中",
    sunriseCityName: "臺中",
  },
  {
    cityNameAPI: "%E6%96%B0%E7%AB%B9%E7%B8%A3",
    cityName: "新竹縣",
    locationName: "新竹",
    sunriseCityName: "新竹",
  },
  {
    cityNameAPI: "%E8%8A%B1%E8%93%AE%E7%B8%A3",
    cityName: "花蓮縣",
    locationName: "花蓮",
    sunriseCityName: "花蓮",
  },
  {
    cityNameAPI: "%E9%80%A3%E6%B1%9F%E7%B8%A3",
    cityName: "連江縣",
    locationName: "馬祖",
    sunriseCityName: "馬祖",
  },
  {
    cityNameAPI: "%E6%BE%8E%E6%B9%96%E7%B8%A3",
    cityName: "澎湖縣",
    locationName: "澎湖",
    sunriseCityName: "澎湖",
  },
  {
    cityNameAPI: "%E6%96%B0%E5%8C%97%E5%B8%82",
    cityName: "新北市",
    locationName: "板橋",
    sunriseCityName: "新北市",
  },
];
export const findLocation = (cityName) => {
  return availableLocations.find((location) => location.cityName === cityName);
};
